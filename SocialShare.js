import React from 'react';
import PropTypes from 'prop-types';

import './SocialShare.scss';

/**
 * SocialShare
 * @description [Description]
 * @example
  <div id="SocialShare"></div>
  <script>
    ReactDOM.render(React.createElement(Components.SocialShare, {
        title : 'Example SocialShare'
    }), document.getElementById("SocialShare"));
  </script>
 */
class SocialShare extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'socialshare';
	}

	render() {
		return (
			<div className={`${this.baseClass}`} ref={component => (this.component = component)}>
				SocialShare
			</div>
		);
	}
}

SocialShare.defaultProps = {
	children: null
};

SocialShare.propTypes = {
	children: PropTypes.node
};

export default SocialShare;
